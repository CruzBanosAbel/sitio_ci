<!-- Main wrap -->
<div id="main_wrap">		
	<!-- Main -->
   	<div id="main">
   		<h2 class="section_title">About</h2><!-- This is your section title -->      				
      	<!-- Content - full width (class="content_full_width") -->
      	<div id="content" class="content_full_width">				
			<p style="text-align: center;"><img class="aligncenter size-full wp-image-1194" title="coffee_oaxaca_nuevo_mundo" src="http://cafenuevomundo.com/wp-content/uploads/2009/12/coffee_oaxaca_nuevo_mundo.jpg" alt="Best Coffee Oaxaca Nuevo Mundo" width="700" height="467" /></p>
			<p style="text-align: left;">Nuevo Mundo Coffee Roaster is a small independent cafe in the heart of the colonial city of Oaxaca in Southern Mexico. We at Nuevo Mundo believe in the concept of sustainable coffee &#8212; that in the face of the global crisis affecting the coffee industry, one key solution is to sustain relationships with small coffee farms that promote organic, shade-grown and fair trade coffee, as part of our search for truly excellent coffees grown in Mexico.</p>
			<p style="text-align: left;">We take pride in being an artisan coffee roaster. We bring to Oaxaca a new world of handcrafted coffee using the very best high grown coffee beans from Mexico, blended and roasted according to the highest standards of science and art, to create the most memorable coffee experience you will ever have in this part of the world.</p>
			<p style="text-align: left;">We roast our coffee in-store daily to provide you with the best and freshest cup of coffee available in Oaxaca. Because we are a micro-roaster, we roast our coffee in small batches every day of the week. From our cafe, we supply select restaurants and coffee shops around Oaxaca as well as coffee lovers from all over the world who drop by in our store.</p>
			<p style="text-align: left;">Whether it&#8217;s coffee-to-go or enjoying your favorite coffee beverage in-house, we offer you the best service and personal attention from our trained baristas. We have a contemporary and comfortable cafe environment with soothing classical music in the early morning and jazz, Mexican and Latin rhythms the rest of the day.</p>
			<p style="text-align: left;">When you drop by for a visit, in addition to enjoying the finest cup of coffee in Oaxaca, we can offer you information on what to see, what to do and where to stay and dine in this beautiful, relaxed, friendly and historic city that we call our home.</p>
			<p style="text-align: left;">Our in-store favorite using the best mountain-grown arabica coffee beans from Chiapas and Oaxaca. For that dark roast chocolately flavor, distinctive crema and superior body, we blend just a touch of Chiapas robusta and sun-dried coffee from Atoyac,Guerrero. This is the blend that is becoming  everybody&#8217;s favorite coffee.</p>
			<p style="text-align: left;">Our favorite drip-coffee blend, using 100 percent mountain grown arabica beans from Chiapas for that extra-rich flavor and aroma and from Pluma Hidalgo, Oaxaca for that smooth, bright and mellow touch. Available in medium or dark roast, whole bean or ground.</p>
			<div id="TA_excellent442" class="TA_excellent">
				<ul id="isKlequF" class="TA_links cJsqEfiEzPu">
					<li id="RVpExtEdg" class="pRiSfSdd"><a target="_blank" href=http://www.tripadvisor.com/Restaurant_Review-g150801-d1575242-Reviews-Nuevo_Mundo_Coffee_Roaster-Oaxaca_Southern_Mexico.html>Nuevo Mundo Coffee Roaster</a> rated &#8220;excellent&#8221; by travelers</li>
				</ul>
			</div>
			<p><script src="http://www.jscache.com/wejs?wtype=excellent&amp;uniq=442&amp;locationId=1575242&amp;lang=en_US"></script></p>
			<iframe class="fblikes" src="http://www.facebook.com/plugins/like.php?href=http://cafenuevomundo.com/about-2/&amp;send=false&amp;layout=standard&amp;width=600&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:600px; height:80px; margin: 0px 0px 0px 0px;" allowTransparency="true"></iframe>						
			<div id="comments">
				<p class="nocomments"></p>
			</div><!-- #comments -->
        </div> 
    </div>
	<?php
		$this->load->view("sitio/footer");
	?>
</div>